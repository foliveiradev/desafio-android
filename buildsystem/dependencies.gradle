allprojects {
    repositories {
        google()
        jcenter()
        maven { url 'https://jitpack.io' }
    }
}

ext {
    kotlin_version = '1.2.41'
    kodein = '4.1.0'

    supportLibrary = '26.1.0'
    databidingCompiler = '3.1.0'
    constraintLayout = '1.0.2'
    okHttp = '3.8.0'
    retrofit2 = '2.3.0'
    rxJava2 = '2.1.8'
    rxJavaAndroid = '2.0.1'
    gson = '2.8.1'
    picasso = '2.5.2'
    circleImageView = '2.2.0'
    room = '1.1.0'

    leakCanary = '1.5.4'

    // Testing
    junit = '4.12'
    mockitoKotlin = '1.5.0'
    assertJ = '3.8.0'
    espresso = '3.0.2'
    supportTest = '1.0.2'

    commonDependencies = [
            kotlinStandardLibrary : "org.jetbrains.kotlin:kotlin-stdlib:${kotlin_version}",
            kodein                : "com.github.salomonbrys.kodein:kodein:${kodein}",
            kodeinAndroid         : "com.github.salomonbrys.kodein:kodein-android:${kodein}",
            kodeinConf            : "com.github.salomonbrys.kodein:kodein-conf:${kodein}",

            supportAppCompatV7    : "com.android.support:appcompat-v7:${supportLibrary}",
            supportDesign         : "com.android.support:design:${supportLibrary}",
            supportCardView       : "com.android.support:cardview-v7:${supportLibrary}",
            supportRecyclerView   : "com.android.support:recyclerview-v7:${supportLibrary}",
            constraintLayout      : "com.android.support.constraint:constraint-layout:${constraintLayout}",
            databindingCompiler   : "com.android.databinding:compiler:${databidingCompiler}",
            picasso               : "com.squareup.picasso:picasso:${picasso}",
            circleImageView       : "de.hdodenhof:circleimageview:${circleImageView}",
            room                  : "android.arch.persistence.room:runtime:${room}",
            roomCompiler          : "android.arch.persistence.room:compiler:${room}",
            roomRx                : "android.arch.persistence.room:rxjava2:${room}",

            okHttp                : "com.squareup.okhttp3:okhttp:${okHttp}",
            okHttpInterceptor     : "com.squareup.okhttp3:logging-interceptor:${okHttp}",

            retrofit2             : "com.squareup.retrofit2:retrofit:${retrofit2}",
            retrofitConverter     : "com.squareup.retrofit2:converter-gson:${retrofit2}",
            retrofitRxJavaAdapter : "com.squareup.retrofit2:adapter-rxjava2:${retrofit2}",

            gson                  : "com.google.code.gson:gson:${gson}",

            rxJava2               : "io.reactivex.rxjava2:rxjava:${rxJava2}",
            rxJavaAndroid         : "io.reactivex.rxjava2:rxandroid:${rxJavaAndroid}",
    ]

    commonTestDependencies = [
            junit   : "junit:junit:${junit}",
            assertj : "org.assertj:assertj-core:${assertJ}",
            mockito : "com.nhaarman:mockito-kotlin-kt1.1:${mockitoKotlin}",
            mockWebServer       : "com.squareup.okhttp3:mockwebserver:${okHttp}",
    ]

    commonAndroidTestDependencies = [
            espressoCore        : "com.android.support.test.espresso:espresso-core:${espresso}",
            espressoContrib     : "com.android.support.test.espresso:espresso-contrib:${espresso}",
            espressoIntents     : "com.android.support.test.espresso:espresso-intents:${espresso}",
            supportTestRunner   : "com.android.support.test:runner:${supportTest}",
            supportTestRules    : "com.android.support.test:rules:${supportTest}",
            supportAnnotations  : "com.android.support:support-annotations:27.1.1",
            orchestrator        : "com.android.support.test:orchestrator:${supportTest}",
    ]

    developmentDependencies = [
            leakcanary    : "com.squareup.leakcanary:leakcanary-android:${leakCanary}",
            leakcanaryNoOp: "com.squareup.leakcanary:leakcanary-android-no-op:${leakCanary}",
            stetho        : "com.facebook.stetho:stetho:1.5.0"
    ]
}