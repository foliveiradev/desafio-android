package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.createRepositoriesFixture
import oliveiradev.com.github.concrete_desafio_android.createRepositoryUIFixture
import oliveiradev.com.github.concrete_desafio_android.data.repositories.GithubRepoRepository
import oliveiradev.com.github.concrete_desafio_android.data.repositories.RepositoryDataSource
import oliveiradev.com.github.concrete_desafio_android.helper.schedulers.ImmediateScheduler
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadBehavior
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Created by felipe on 25/02/18.
 */
class RepositoryPresenterTest {

    private lateinit var presenter: RepositoryPresenter
    private lateinit var mapper: RepositoryUIMapper

    @Mock
    private lateinit var repositoryView: RepositoryView
    @Mock
    private lateinit var githubRemoteDataSource: RepositoryDataSource
    @Mock
    private lateinit var githubLocalDataSource: RepositoryDataSource
    private lateinit var githubRepoRepository: GithubRepoRepository

    @Before
    fun setUp() {

        MockitoAnnotations.initMocks(this)
        githubRepoRepository = GithubRepoRepository(githubRemoteDataSource, githubLocalDataSource)
        mapper = RepositoryUIMapper()
        presenter = RepositoryPresenter(
            githubRepoRepository,
            mapper,
            LoadBehavior(repositoryView),
            ImmediateScheduler()
        )
        presenter.attachView(repositoryView)
    }

    @After
    fun tearDown() {
        presenter.detachView()
    }

    @Test
    fun `should get repositories and put it into the view`() {
        val repositories = createRepositoriesFixture()
        whenever(githubRemoteDataSource.getRepositories(1)).thenReturn(Flowable.fromIterable(repositories))

        presenter.getRepositories()

        verify(repositoryView).showLoading()
        repositories.forEach {
            verify(repositoryView).addItemOnList(mapper.apply(it))
        }
        verify(repositoryView).dismissEmptyState()
        verify(repositoryView).dismissLoading()
    }

    @Test
    fun `should get repositories with error and show error message`() {
        whenever(githubRemoteDataSource.getRepositories(1))
            .thenReturn(Flowable.error(Exception("some error")))

        presenter.getRepositories()

        verify(repositoryView).showLoading()
        verify(repositoryView).dismissLoading()
        verify(repositoryView).showGetRepositoriesError()
    }

    @Test
    fun `should get empty repositories and show empty state`() {
        whenever(githubRemoteDataSource.getRepositories(1)).thenReturn(Flowable.empty())

        presenter.getRepositories()

        verify(repositoryView).showLoading()
        verify(repositoryView).showEmptyState()
        verify(repositoryView).dismissLoading()
    }

    @Test
    fun `When take repositories should dismiss empty state`() {
        val repositories = createRepositoriesFixture()
        whenever(githubRemoteDataSource.getRepositories(1)).thenReturn(Flowable.fromIterable(repositories))

        presenter.getRepositories()

        verify(repositoryView).showLoading()
        verify(repositoryView).dismissEmptyState()
        verify(repositoryView).dismissLoading()
    }

    @Test
    fun `should click on list item and navigate to pull requests`() {
        val repositoryUI = createRepositoryUIFixture()

        presenter.navigateToPullRequests(repositoryUI)

        verify(repositoryView).navigateToPullRequests(repositoryUI)
    }
}