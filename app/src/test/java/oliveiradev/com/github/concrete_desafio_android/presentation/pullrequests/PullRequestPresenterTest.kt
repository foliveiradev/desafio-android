package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.createPRUIFixture
import oliveiradev.com.github.concrete_desafio_android.createPRsFixture
import oliveiradev.com.github.concrete_desafio_android.createRepositoryUIFixture
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.PullRequestDataSource
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.PullRequestRepository
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestResponse
import oliveiradev.com.github.concrete_desafio_android.helper.schedulers.ImmediateScheduler
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadBehavior
import oliveiradev.com.github.concrete_desafio_android.presentation.repositories.RepositoryUIModel
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PullRequestPresenterTest {

    private lateinit var pullRequestPresenter: PullRequestPresenter
    @Mock
    private lateinit var  pullRequestView: PullRequestView

    @Mock
    private lateinit var pullRequestDataSource: PullRequestDataSource
    private lateinit var pullRequestRepository: PullRequestRepository
    private lateinit var mapper: PullRequestUIMapper

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mapper = PullRequestUIMapper()
        pullRequestRepository = PullRequestRepository(pullRequestDataSource)
        pullRequestPresenter = PullRequestPresenter(
            pullRequestRepository,
            mapper,
            LoadBehavior(pullRequestView),
            ImmediateScheduler()
        )

        pullRequestPresenter.attachView(pullRequestView)
    }

    @Test
    fun `When take PRs with success should say to view that items are coming`() {
        val PRs = createPRsFixture()
        val repositoryUI = createRepositoryUIFixture()
        getPRs(repositoryUI, PRs)

        pullRequestPresenter.getPullRequests(repositoryUI)

        verify(pullRequestView).showLoading()
        PRs.forEach {
            verify(pullRequestView).addItemOnList(mapper.apply(it))
        }
        verify(pullRequestView).putOpenedPRQuantity(PRs.size)
        verify(pullRequestView).dismissLoading()
    }

    @Test
    fun `When take PRs fail should show an error`() {
        val repositoryUI = createRepositoryUIFixture()
        getPRs(repositoryUI, isSuccess = false)

        pullRequestPresenter.getPullRequests(repositoryUI)

        verify(pullRequestView).showLoading()
        verify(pullRequestView).dismissLoading()
        verify(pullRequestView).showGetPullRequestsError()
    }

    @Test
    fun `When take PRs with success and list size is zero should show empty state`() {
        val repositoryUI = createRepositoryUIFixture()
        getPRs(repositoryUI)

        pullRequestPresenter.getPullRequests(repositoryUI)

        verify(pullRequestView).showLoading()
        verify(pullRequestView).dismissLoading()
        verify(pullRequestView).showEmptyState()
    }

    @Test
    fun `When take PRs with success and list size is greater than zero should show dismiss state`() {
        val PRs = createPRsFixture()
        val repositoryUI = createRepositoryUIFixture()
        getPRs(repositoryUI, PRs)

        pullRequestPresenter.getPullRequests(repositoryUI)

        verify(pullRequestView).showLoading()
        verify(pullRequestView).dismissLoading()
        verify(pullRequestView).dismissEmptyState()
    }

    @Test
    fun `When click on PR item should open the browser`() {
        val PRUIModel = createPRUIFixture()

        pullRequestPresenter.navigateToPullRequestBrowser(PRUIModel)

        verify(pullRequestView).navigateToPullRequestBrowser(PRUIModel.webUrl)
    }

    private fun getPRs(
        repositoryUIModel: RepositoryUIModel,
        PRs: List<PullRequestResponse> = emptyList(),
        isSuccess: Boolean = true
    ) {
        val stubbing = whenever(
            pullRequestDataSource.getPullRequests(
                repositoryUIModel.ownerName,
                repositoryUIModel.name
            )
        )

        if (isSuccess) {
            stubbing.thenReturn(Flowable.fromIterable(PRs))
        } else {
            stubbing.thenReturn(Flowable.error(Exception("some error")))
        }
    }
}