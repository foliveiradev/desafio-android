package oliveiradev.com.github.concrete_desafio_android.data.repositories.network

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.mockwebserver.MockWebServer
import oliveiradev.com.github.concrete_desafio_android.createHttpClient
import oliveiradev.com.github.concrete_desafio_android.data.ServiceFactory
import oliveiradev.com.github.concrete_desafio_android.helpers.MockedResponse
import oliveiradev.com.github.concrete_desafio_android.helpers.newMockedCall
import org.junit.After
import org.junit.Before

import org.junit.Test
import retrofit2.HttpException

class RepositoryServiceTest {

    private lateinit var mockServer: MockWebServer
    private lateinit var repositoryService: RepositoryService

    @Before
    fun setUp() {
        mockServer = MockWebServer()
        mockServer.start()
        val api = ServiceFactory.create<RepositoryAPI>(mockServer.url("/").toString(), createHttpClient())
        repositoryService = RepositoryService(api)
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

    @Test
    fun `When take repositories from api with success should iterate on a Flowable`() {
        val fakeRepositories = MockedResponse.fakeRepositories()
        val type = object : TypeToken<ItemsResponse<RepositoryResponse>>(){}.type
        val deserializedResponse = Gson().fromJson<ItemsResponse<RepositoryResponse>>(fakeRepositories, type)
        mockServer.newMockedCall(200, fakeRepositories)

        repositoryService.getRepositories(1)
            .test()
            .assertSubscribed()
            .assertValueCount(1)
            .assertValueSequence(deserializedResponse.items)
            .assertComplete()
    }

    @Test
    fun `When take empty repositories from api with success should emit no values`() {
        val fakeEmptyRepositories = MockedResponse.fakeEmptyRepositories()
        mockServer.newMockedCall(200, fakeEmptyRepositories)

        repositoryService.getRepositories(1)
            .test()
            .assertSubscribed()
            .assertNoValues()
            .assertComplete()
    }

    @Test
    fun `When api call fail should emit error`() {
        mockServer.newMockedCall(500, "")

        repositoryService.getRepositories(1)
            .test()
            .assertSubscribed()
            .assertError(HttpException::class.java)
    }
}