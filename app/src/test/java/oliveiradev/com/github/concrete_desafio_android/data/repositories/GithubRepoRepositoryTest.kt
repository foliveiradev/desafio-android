package oliveiradev.com.github.concrete_desafio_android.data.repositories

import android.accounts.NetworkErrorException
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.createRepositoriesFixture
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Created by felipe on 25/02/18.
 */
class GithubRepoRepositoryTest {

    private lateinit var githubRepoRepository: GithubRepoRepository

    @Mock
    private lateinit var remoteRepositoryDataSource: RepositoryDataSource
    @Mock
    private lateinit var localRepositoryDataSource: RepositoryDataSource

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        githubRepoRepository = GithubRepoRepository(remoteRepositoryDataSource, localRepositoryDataSource)
    }

    @Test
    fun `When call repositories with success and without cache should call remote data source`() {
        val repositories = createRepositoriesFixture()
        whenever(localRepositoryDataSource.itemsCount).thenReturn(0)
        whenever(remoteRepositoryDataSource.getRepositories(1))
            .thenReturn(Flowable.fromIterable(repositories))

        githubRepoRepository.getRepositories(1)
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValueSequence(repositories)
    }

    @Test
    fun `When call repositories with success and with cache should call local data source`() {
        val repositories = createRepositoriesFixture()
        whenever(localRepositoryDataSource.itemsCount).thenReturn(1)
        whenever(localRepositoryDataSource.getRepositories(1))
            .thenReturn(Flowable.fromIterable(repositories))

        githubRepoRepository.getRepositories(1)
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValueSequence(repositories)
    }

    @Test
    fun `When call repositories with success and with cache should call remote data source and save on cache`() {
        val repositories = createRepositoriesFixture()
        whenever(localRepositoryDataSource.itemsCount).thenReturn(0)
        whenever(remoteRepositoryDataSource.getRepositories(1))
            .thenReturn(Flowable.fromIterable(repositories))

        githubRepoRepository.getRepositories(1)
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValueSequence(repositories)

        repositories.forEach {
            verify(localRepositoryDataSource).saveInternal(it)
        }
    }

    @Test
    fun `When take repositories with network error should pass the error forward`() {
        val exception = NetworkErrorException("some network fail")
        whenever(remoteRepositoryDataSource.getRepositories(1))
            .thenReturn(Flowable.error(exception))

        githubRepoRepository.getRepositories(1)
            .test()
            .assertError(exception)
    }
}