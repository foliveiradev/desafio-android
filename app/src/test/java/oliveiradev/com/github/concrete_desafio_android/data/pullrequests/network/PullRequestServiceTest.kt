package oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.mockwebserver.MockWebServer
import oliveiradev.com.github.concrete_desafio_android.createHttpClient
import oliveiradev.com.github.concrete_desafio_android.data.ServiceFactory
import oliveiradev.com.github.concrete_desafio_android.helpers.MockedResponse
import oliveiradev.com.github.concrete_desafio_android.helpers.newMockedCall
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException

class PullRequestServiceTest {

    private lateinit var mockServer: MockWebServer
    private lateinit var PRService: PullRequestService
    private val fakeRepoName = "some repo"
    private val fakeOwnerRepoName = "some owner"

    @Before
    fun setUp() {
        mockServer = MockWebServer()
        mockServer.start()
        val api = ServiceFactory.create<PullRequestAPI>(mockServer.url("/").toString(), createHttpClient())
        PRService = PullRequestService(api)
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

    @Test
    fun `When take repositories from api with success should iterate on a Flowable`() {
        val fakePRs = MockedResponse.fakePullRequests()
        val type = object : TypeToken<List<PullRequestResponse>>(){}.type
        val deserializedResponse = Gson().fromJson<List<PullRequestResponse>>(fakePRs, type)
        mockServer.newMockedCall(200, fakePRs)

        PRService.getPullRequests(fakeOwnerRepoName, fakeRepoName)
            .test()
            .assertSubscribed()
            .assertValueCount(1)
            .assertValueSequence(deserializedResponse)
            .assertComplete()
    }

    @Test
    fun `When take empty repositories from api with success should emit no values`() {
        val fakeEmptyPRs = MockedResponse.fakeEmptyPullRequests()
        mockServer.newMockedCall(200, fakeEmptyPRs)

        PRService.getPullRequests(fakeOwnerRepoName, fakeRepoName)
            .test()
            .assertSubscribed()
            .assertNoValues()
            .assertComplete()
    }

    @Test
    fun `When api call fail should emit error`() {
        mockServer.newMockedCall(500, "")

        PRService.getPullRequests(fakeOwnerRepoName, fakeRepoName)
            .test()
            .assertSubscribed()
            .assertError(HttpException::class.java)
    }
}