package oliveiradev.com.github.concrete_desafio_android.data.repositories.local

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import oliveiradev.com.github.concrete_desafio_android.createRepositoriesEntity
import oliveiradev.com.github.concrete_desafio_android.data.GithubDataBase
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RepositoryLocalServiceTest {

    private lateinit var repositoryDAO: RepositoryDAO
    private lateinit var db: GithubDataBase

    @Before
    fun setup() {
        val context = InstrumentationRegistry.getContext()
        db = Room.inMemoryDatabaseBuilder(context, GithubDataBase::class.java).build()
        repositoryDAO = db.getRepositoryDAO()
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun whenGetRepositoriesShouldAddIntoLocalStorage() {
        val entities = createRepositoriesEntity()
        insertRepositories(entities)

        val rows = repositoryDAO.repositorySize()

        assertThat(rows, equalTo(entities.size))
    }

    @Test
    fun whenGetRepositoriesFromLocalStorageShouldOrderedFromStars() {
        val entities = createRepositoriesEntity()
        insertRepositories(entities)

        val repositories = repositoryDAO.getRepositories().blockingGet()

        assertThat(entities, equalTo(repositories.sortedWith(compareBy { it.starsCount })))
    }

    private fun insertRepositories(items: List<RepositoryEntity>) {
        items.forEach {
            repositoryDAO.addRepository(it)
        }
    }
}