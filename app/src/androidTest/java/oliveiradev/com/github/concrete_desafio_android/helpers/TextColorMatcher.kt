package oliveiradev.com.github.concrete_desafio_android.helpers

import android.support.annotation.ColorInt
import android.support.test.espresso.matcher.BoundedMatcher
import android.view.View
import android.widget.TextView
import org.hamcrest.Description
import org.hamcrest.Matcher

object TextColorMatcher {

    fun withTextColor(@ColorInt expectedColor: Int): Matcher<View> {
        return object : BoundedMatcher<View, TextView>(TextView::class.java) {
            internal var currentColor = 0

            override fun describeTo(description: Description) {
                description.appendText("expected TextColor: ")
                    .appendValue(Integer.toHexString(expectedColor))
                description.appendText(" current TextColor: ")
                    .appendValue(Integer.toHexString(currentColor))
            }

            override fun matchesSafely(item: TextView): Boolean {
                if (currentColor == 0)
                    currentColor = item.currentTextColor
                return currentColor == expectedColor
            }
        }
    }
}
