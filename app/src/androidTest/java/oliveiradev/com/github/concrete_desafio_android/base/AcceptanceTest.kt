package oliveiradev.com.github.concrete_desafio_android.base

import android.app.Activity
import android.support.test.InstrumentationRegistry
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.github.salomonbrys.kodein.Kodein
import oliveiradev.com.github.concrete_desafio_android.asApp
import org.junit.Rule
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
abstract class AcceptanceTest<T : Activity>(clazz: Class<T>) {


    @get:Rule
    val activityRule: ActivityTestRule<T> = ActivityTestRule(clazz, true, false)
    val app = InstrumentationRegistry.getInstrumentation().targetContext.asApp()

    abstract val testDependencies: Kodein.Module
}