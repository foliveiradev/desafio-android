package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasAction
import android.support.test.espresso.matcher.RootMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v4.content.ContextCompat
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import okhttp3.mockwebserver.MockWebServer
import oliveiradev.com.github.concrete_desafio_android.R
import oliveiradev.com.github.concrete_desafio_android.base.AcceptanceTest
import oliveiradev.com.github.concrete_desafio_android.data.ServiceFactory
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestAPI
import oliveiradev.com.github.concrete_desafio_android.helpers.MockedResponse
import oliveiradev.com.github.concrete_desafio_android.helpers.TextColorMatcher
import oliveiradev.com.github.concrete_desafio_android.helpers.newMockedCall
import org.hamcrest.Matchers
import org.hamcrest.Matchers.not
import org.hamcrest.core.AllOf.allOf
import org.junit.After
import org.junit.Before
import org.junit.Test

class PullRequestsActivityTest :
    AcceptanceTest<PullRequestsActivity>(PullRequestsActivity::class.java) {

    private lateinit var mockServer: MockWebServer

    override val testDependencies: Kodein.Module
        get() = Kodein.Module {
            bind<PullRequestAPI>(overrides = true) with provider {
                ServiceFactory.create<PullRequestAPI>(mockServer.url("/").toString(), instance())
            }
        }

    @Before
    fun setup() {
        mockServer = MockWebServer()
        mockServer.start()
        app.kodein.addImport(testDependencies, allowOverride = true)
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

    @Test
    fun whenReceiveRepository_shouldShowRepoNameOnActionBar() {
        activityRule.launchActivity(createIntent())
        onView(withText("RxJava")).check(matches(isDisplayed()))
    }

    @Test
    fun whenTakePRsWithSuccess_shouldShowQuantityOfOpenedPRs() {
        mockServer.newMockedCall(200, MockedResponse.fakePullRequests())
        activityRule.launchActivity(createIntent())
        onView(withText("1 opened")).check(matches(isDisplayed()))
    }

    @Test
    fun whenEmptyPRs_shouldShowEmptyState() {
        mockServer.newMockedCall(200, MockedResponse.fakeEmptyPullRequests())
        activityRule.launchActivity(createIntent())
        onView(withText(R.string.pull_request_empty_message)).check(matches(isDisplayed()))
    }

    @Test
    fun whenTakePRsWithSuccess_shouldShowThePRsList() {
        mockServer.newMockedCall(200, MockedResponse.fakePullRequests())
        activityRule.launchActivity(createIntent())

        onView(withText("1 opened")).check(matches(isDisplayed()))
        checkPRItem()
        onView(withText(R.string.pull_request_empty_message)).check(matches(not(isDisplayed())))
    }

    @Test
    fun whenConfigurationChange_shouldHoldTheCurrentList() {
        mockServer.newMockedCall(200, MockedResponse.fakePullRequests())
        activityRule.launchActivity(createIntent())
        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        onView(withText("1 opened")).check(matches(isDisplayed()))
        checkPRItem()
    }

    @Test
    fun whenTakeErrorResult_shouldShowAToastMessage() {
        mockServer.newMockedCall(500, "")
        activityRule.launchActivity(createIntent())
        onView(withText(R.string.pull_request_error)).inRoot(
            RootMatchers.withDecorView(
                not(
                    Matchers.`is`(
                        activityRule.activity.window.decorView
                    )
                )
            )
        ).check(
            matches(isDisplayed()))
    }

    @Test
    fun whenClickOnPR_shouldOpenPRPageOnBrowser() {
        mockServer.newMockedCall(200, MockedResponse.fakePullRequests())
        activityRule.launchActivity(createIntent())

        Intents.init()
        intending(hasAction(Intent.ACTION_VIEW))
            .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, Intent()))
        onView(withId(R.id.container)).perform(click())
        intended(hasAction(Intent.ACTION_VIEW))
        Intents.release()
    }

    private fun createIntent(): Intent = Intent().apply {
        putExtra(
            PullRequestsActivity.REPOSITORY_EXTRA,
            MockedResponse.fakeSingleRepositoryUIModel()
        )
    }

    private fun checkPRItem() {
        onView(
            allOf(
                withId(R.id.title),
                withText("2.x: readme: mention 1.x eol; 2.x snapshot javadoc url"),
                TextColorMatcher.withTextColor(
                    ContextCompat.getColor(
                        activityRule.activity,
                        R.color.blueColor
                    )
                )
            )
        ).check(matches(isDisplayed()))
        onView(
            allOf(
                withText("akarnokd")
            )
        ).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.pr_owner_avatar), hasSibling(withText("akarnokd"))))
    }
}