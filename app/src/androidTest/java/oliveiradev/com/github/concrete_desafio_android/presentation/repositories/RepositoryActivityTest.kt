package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.content.pm.ActivityInfo
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.Intents.intending
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v4.content.ContextCompat
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import okhttp3.mockwebserver.MockWebServer
import oliveiradev.com.github.concrete_desafio_android.R
import oliveiradev.com.github.concrete_desafio_android.base.AcceptanceTest
import oliveiradev.com.github.concrete_desafio_android.data.ServiceFactory
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryAPI
import oliveiradev.com.github.concrete_desafio_android.helpers.MockedResponse
import oliveiradev.com.github.concrete_desafio_android.helpers.TextColorMatcher
import oliveiradev.com.github.concrete_desafio_android.helpers.newMockedCall
import oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests.PullRequestsActivity
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not
import org.hamcrest.core.AllOf.allOf
import org.junit.After
import org.junit.Before
import org.junit.Test


class RepositoryActivityTest : AcceptanceTest<RepositoryActivity>(RepositoryActivity::class.java){

    private lateinit var mockServer: MockWebServer

    override val testDependencies: Kodein.Module
        get() = Kodein.Module {
            bind<RepositoryAPI>(overrides = true) with provider {
                ServiceFactory.create<RepositoryAPI>(mockServer.url("/").toString(), instance())
            }
        }

    @Before
    fun setup() {
        mockServer = MockWebServer()
        mockServer.start()
        app.kodein.addImport(testDependencies, allowOverride = true)
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

    @Test
    fun whenTakeRepositoriesWithSuccess_shouldShowItems() {
        mockServer.newMockedCall(200, MockedResponse.fakeRepositories())
        activityRule.launchActivity(Intent())
        onView(withId(R.id.repositories)).check(matches(isDisplayed()))
        checkRepositoryItem()
    }

    @Test
    fun whenTakeEmptyRepositories_shouldShowEmptyState() {
        mockServer.newMockedCall(200, MockedResponse.fakeEmptyRepositories())
        activityRule.launchActivity(Intent())
        onView(withId(R.id.empty)).check(matches(isDisplayed()))
    }

    @Test
    fun whenTakeErrorResult_shouldShowAToastMessage() {
        mockServer.newMockedCall(500, "")
        activityRule.launchActivity(Intent())
        onView(withText(R.string.repository_error)).inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView)))).check(
            matches(isDisplayed()))
    }

    @Test
    fun whenClickOnItemList_shouldStartPullRequestsActivity_withExtra() {
        mockServer.newMockedCall(200, MockedResponse.fakeRepositories())
        activityRule.launchActivity(Intent())
        Intents.init()
        val matcher = allOf<Intent>(
            hasComponent(PullRequestsActivity::class.java.name),
            hasExtraWithKey(PullRequestsActivity.REPOSITORY_EXTRA)
        )

        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, null)

        intending(matcher).respondWith(result)

        onView(withId(R.id.repositories)).perform(actionOnItemAtPosition<RepositoryViewHolder>(0, click()))

        intended(matcher)
        Intents.release()
    }

    @Test
    fun whenConfigurationChange_shouldHoldTheCurrentList() {
        mockServer.newMockedCall(200, MockedResponse.fakeRepositories())
        activityRule.launchActivity(Intent())
        activityRule.activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        onView(withId(R.id.repositories)).check(matches(isDisplayed()))
        checkRepositoryItem()
    }

    private fun checkRepositoryItem() {
        onView(allOf(withId(R.id.avatar), hasSibling(withText("RxJava")))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.repo_name), withText("RxJava"))).check(matches(isDisplayed()))
        onView(allOf(
            withId(R.id.stars),
            withText("32827"),
            TextColorMatcher.withTextColor(ContextCompat.getColor(activityRule.activity, R.color.accentColor))
        )).check(matches(isDisplayed()))
        onView(allOf(
            withId(R.id.forks),
            withText("5755"),
            TextColorMatcher.withTextColor(ContextCompat.getColor(activityRule.activity, R.color.accentColor))
        )).check(matches(isDisplayed()))
    }
}