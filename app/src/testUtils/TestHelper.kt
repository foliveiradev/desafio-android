package oliveiradev.com.github.concrete_desafio_android

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestResponse
import oliveiradev.com.github.concrete_desafio_android.data.repositories.local.RepositoryEntity
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.OwnerResponse
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse
import oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests.PullRequestUIModel
import oliveiradev.com.github.concrete_desafio_android.presentation.repositories.RepositoryUIModel
import java.util.*

/**
 * Created by felipe on 25/02/18.
 */
fun createRepositoriesFixture(): List<RepositoryResponse> {
    return listOf(
        RepositoryResponse(
            1,
            "name1",
            "description1",
            1,
            1,
            OwnerResponse("name1", "avatar1")
        )
    )
}

fun createPRsFixture(): List<PullRequestResponse> {
    return listOf(
        PullRequestResponse(
            "title",
            "body",
            Date(),
            OwnerResponse("name1", "avatar1"),
            "url"
        )
    )
}

fun createRepositoryUIFixture(): RepositoryUIModel {
    return RepositoryUIModel("name", "description", "1", "1", "ownerName", "avatar")
}

fun createPRUIFixture(): PullRequestUIModel {
    return PullRequestUIModel("title", "body", "01/01/1991", "avatar", "name", "url")
}

fun createRepositoriesEntity(): List<RepositoryEntity> {
    return listOf(
        RepositoryEntity(1, "name", "description", 1,
            1, "login", "avatar"),
        RepositoryEntity(12, "name2", "description2", 12,
            12, "login2", "avatar2")
    )
}

fun createHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        .build()
}