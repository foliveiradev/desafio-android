package oliveiradev.com.github.concrete_desafio_android.helpers

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer

fun MockWebServer.newMockedCall(fakeHttpStatus: Int, fakeHttpBody: String) {
    this.enqueue(MockResponse().setResponseCode(fakeHttpStatus).setBody(fakeHttpBody))
}