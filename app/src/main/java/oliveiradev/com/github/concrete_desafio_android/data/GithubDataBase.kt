package oliveiradev.com.github.concrete_desafio_android.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import oliveiradev.com.github.concrete_desafio_android.data.repositories.local.RepositoryDAO
import oliveiradev.com.github.concrete_desafio_android.data.repositories.local.RepositoryEntity

@Database(entities = arrayOf(RepositoryEntity::class), version = 1)
abstract class GithubDataBase : RoomDatabase() {

    abstract fun getRepositoryDAO(): RepositoryDAO

    companion object {

        private var INSTANCE: GithubDataBase? = null

        private val lock = Any()

        fun getInstance(context: Context): GithubDataBase {
            synchronized(lock) {
                return INSTANCE ?: createDb(context)
            }
        }

        private fun createDb(context: Context): GithubDataBase {
            return Room.databaseBuilder(
                context.applicationContext,
                GithubDataBase::class.java, "Github.db"
            ).build()
        }
    }
}