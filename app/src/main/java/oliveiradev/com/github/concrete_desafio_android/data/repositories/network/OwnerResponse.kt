package oliveiradev.com.github.concrete_desafio_android.data.repositories.network

import com.google.gson.annotations.SerializedName

/**
 * Created by felipe on 19/02/18.
 */
data class OwnerResponse(
        val login: String,
        @SerializedName("avatar_url") val avatar: String
)