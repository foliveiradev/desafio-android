package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

/**
 * Created by felipe on 23/02/18.
 */
interface PullRequestErrorView {

    // I could treat it better
    fun showGetPullRequestsError()
}