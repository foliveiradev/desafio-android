package oliveiradev.com.github.concrete_desafio_android.presentation.base;

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import java.lang.ref.WeakReference

abstract class BasePresenter<View> : LifecycleObserver {

    private var viewReference: WeakReference<View>? = null
    protected var view: View? = null

    open fun attachView(viewContract: View) {
        if (viewReference == null) {
            viewReference = WeakReference(viewContract)
        } else {
            attachInternal(viewContract)
        }

        view = viewReference?.get()
    }

    open fun detachView() {
        viewReference?.clear()
        viewReference = null
    }

    private fun attachInternal(view: View) {
        viewReference?.clear()
        viewReference = WeakReference(view)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    open fun start() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    open fun stop() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        detachView()
    }
}