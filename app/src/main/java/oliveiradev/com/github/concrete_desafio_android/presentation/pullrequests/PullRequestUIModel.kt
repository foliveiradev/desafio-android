package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by felipe on 20/02/18.
 */
data class PullRequestUIModel(
    val title: String,
    val body: String,
    val createdAt: String,
    val prOwnerAvatar: String,
    val prOwnerUsername: String,
    val webUrl: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(body)
        parcel.writeString(createdAt)
        parcel.writeString(prOwnerAvatar)
        parcel.writeString(prOwnerUsername)
        parcel.writeString(webUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PullRequestUIModel> {
        override fun createFromParcel(parcel: Parcel): PullRequestUIModel {
            return PullRequestUIModel(parcel)
        }

        override fun newArray(size: Int): Array<PullRequestUIModel?> {
            return arrayOfNulls(size)
        }
    }
}