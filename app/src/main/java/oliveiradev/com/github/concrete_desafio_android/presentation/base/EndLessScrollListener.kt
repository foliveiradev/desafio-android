package oliveiradev.com.github.concrete_desafio_android.presentation.base

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log


/**
 * Created by felipe on 23/02/18.
 */
abstract class EndLessScrollListener(
    private val layoutManager: RecyclerView.LayoutManager
) : RecyclerView.OnScrollListener() {

    private val visibleThreshold = 5
    private var currentPage: Int = 1
    private var previousTotalItemCount = 0
    private var loading = true
    private var startingPageIndex = 1

    override fun onScrolled(view: RecyclerView?, dx: Int, dy: Int) {
        val totalItemCount = layoutManager.itemCount

        if (totalItemCount < visibleThreshold) return

        val lastVisibleItemPosition = when(layoutManager) {
            is GridLayoutManager -> layoutManager.findLastVisibleItemPosition()

            is LinearLayoutManager -> layoutManager.findLastVisibleItemPosition()

            else -> 0
        }

        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex
            this.previousTotalItemCount = totalItemCount
            if (totalItemCount == 1) {
                this.loading = true
            }
        }

        if (loading && totalItemCount > previousTotalItemCount) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        loadMore(loading, lastVisibleItemPosition + visibleThreshold > totalItemCount)
    }

    private fun loadMore(isLoading: Boolean, isPositionToLoadMore: Boolean) {
        if (isLoading.not() && isPositionToLoadMore) {
            currentPage++
            onLoadMore(currentPage)
            loading = true
        }
    }

    abstract fun onLoadMore(page: Int)
}