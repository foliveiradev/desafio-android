package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import android.util.Log
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import oliveiradev.com.github.concrete_desafio_android.data.repositories.GithubRepoRepository
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse
import oliveiradev.com.github.concrete_desafio_android.helper.fromIOToMainThread
import oliveiradev.com.github.concrete_desafio_android.helper.schedulers.BaseScheduler
import oliveiradev.com.github.concrete_desafio_android.presentation.base.BasePresenter
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadBehavior

/**
 * Created by felipe on 18/02/18.
 */
class RepositoryPresenter(
    private val githubRepoRepository: GithubRepoRepository,
    private val repositoryUIMapper: RepositoryUIMapper,
    private val loadBehavior: LoadBehavior<RepositoryResponse>,
    private val schedulerProvider: BaseScheduler
) : BasePresenter<RepositoryView>() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getRepositories(page: Int = 1) {
        compositeDisposable.add(
            Single.just(githubRepoRepository)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.io())
                .flatMapPublisher { it.getRepositories(page) }
                .observeOn(schedulerProvider.ui())
                .compose(loadBehavior)
                .map(repositoryUIMapper)
                .doOnNext { view?.addItemOnList(it) }
                .toList()
                .subscribe(
                    { checkRepositoriesSize(it) },
                    { view?.showGetRepositoriesError() }
                )
        )
    }

    private fun checkRepositoriesSize(repositories: List<RepositoryUIModel>) {
        if (repositories.isEmpty()) {
            view?.showEmptyState()
        } else {
            view?.dismissEmptyState()
        }
    }

    fun navigateToPullRequests(repositoryUIModel: RepositoryUIModel) {
        view?.navigateToPullRequests(repositoryUIModel)
    }

    override fun stop() {
        compositeDisposable.clear()
    }
}