package oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network

import com.google.gson.annotations.SerializedName
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.OwnerResponse
import java.util.*

/**
 * Created by felipe on 20/02/18.
 */
data class PullRequestResponse(
    val title: String,
    val body: String,
    @SerializedName("created_at") val createdAt: Date,
    val user: OwnerResponse,
    @SerializedName("html_url") val webUrl: String
)