package oliveiradev.com.github.concrete_desafio_android.presentation.base

import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import org.reactivestreams.Publisher

/**
 * Created by felipe on 20/02/18.
 */
class LoadBehavior<T>(private val loadView: LoadView?) : FlowableTransformer<T, T> {

    override fun apply(upstream: Flowable<T>): Publisher<T> {
        return upstream
            .doOnSubscribe { loadView?.showLoading() }
            .doOnTerminate { loadView?.dismissLoading() }
    }
}