package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import android.databinding.DataBindingUtil
import android.databinding.ObservableBoolean
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import oliveiradev.com.github.concrete_desafio_android.R
import oliveiradev.com.github.concrete_desafio_android.databinding.ActivityRepositoriesBinding
import oliveiradev.com.github.concrete_desafio_android.presentation.base.EndLessScrollListener
import oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests.PullRequestsActivity

class RepositoryActivity : AppCompatActivity(), RepositoryView {

    private val kodein = LazyKodein(appKodein)
    private val binding: ActivityRepositoriesBinding by lazy {
        DataBindingUtil.setContentView<ActivityRepositoriesBinding>(
            this,
            R.layout.activity_repositories
        )
    }
    private val presenter by kodein.with(this).instance<RepositoryPresenter>()
    private val repositoriesAdapter by lazy {
        RepositoryAdapter {
            presenter.navigateToPullRequests(
                it
            )
        }
    }
    private val loadingObserver: ObservableBoolean by lazy { ObservableBoolean(true) }
    private val emptyStateObserver: ObservableBoolean by lazy { ObservableBoolean(false) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(presenter)
        setupView(binding)

        if (savedInstanceState == null) {
            presenter.apply {
                attachView(this@RepositoryActivity)
                getRepositories()
            }
        } else {
            presenter.attachView(this)
            repositoriesAdapter.addAll(
                savedInstanceState.getParcelableArrayList(REPOSITORIES_STATE)
            )
            loadingObserver.set(false)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(
            REPOSITORIES_STATE,
            repositoriesAdapter.getItems()
        )
    }

    override fun addItemOnList(repositoryUIModel: RepositoryUIModel) {
        repositoriesAdapter.addItem(repositoryUIModel)
    }

    override fun navigateToPullRequests(repositoryUIModel: RepositoryUIModel) {
        startActivity(PullRequestsActivity.createIntent(this, repositoryUIModel))
    }

    override fun showGetRepositoriesError() {
        Toast.makeText(this, R.string.repository_error, Toast.LENGTH_LONG).show()
    }

    override fun showLoading() {
        loadingObserver.set(true)
    }

    override fun dismissLoading() {
        loadingObserver.set(false)
    }

    override fun showEmptyState() {
        emptyStateObserver.set(true)
    }

    override fun dismissEmptyState() {
        emptyStateObserver.set(false)
    }

    private fun setupView(binding: ActivityRepositoriesBinding) = with(binding) {
        isLoading = loadingObserver
        isEmptyState = emptyStateObserver
        repositories.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@RepositoryActivity)
            adapter = repositoriesAdapter
            addOnScrollListener(createEndLessScrollListener(layoutManager))
        }
    }

    private fun createEndLessScrollListener(layoutManager: RecyclerView.LayoutManager): EndLessScrollListener {
        return object: EndLessScrollListener(layoutManager) {
            override fun onLoadMore(page: Int) {
                presenter.getRepositories(page)
            }
        }
    }

    companion object {

        private const val REPOSITORIES_STATE = "repositoriesState"
    }
}
