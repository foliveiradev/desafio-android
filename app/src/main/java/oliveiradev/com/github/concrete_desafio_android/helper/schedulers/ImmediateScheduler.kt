package oliveiradev.com.github.concrete_desafio_android.helper.schedulers

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

/**
 * Created by felipe on 25/02/18.
 */
class ImmediateScheduler : BaseScheduler {

    override fun io(): Scheduler = Schedulers.trampoline()

    override fun ui(): Scheduler = Schedulers.trampoline()
}