package oliveiradev.com.github.concrete_desafio_android.presentation.base

/**
 * Created by felipe on 22/02/18.
 */
interface EmptyStateView {

    fun showEmptyState()
    fun dismissEmptyState()
}