package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import io.reactivex.functions.Function
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestResponse
import oliveiradev.com.github.concrete_desafio_android.helper.format

/**
 * Created by felipe on 18/02/18.
 */
class PullRequestUIMapper : Function<PullRequestResponse, PullRequestUIModel> {

    override fun apply(response: PullRequestResponse): PullRequestUIModel {
        return PullRequestUIModel(
            response.title,
            response.body,
            response.createdAt.format("dd/MM/yyyy"),
            response.user.avatar,
            response.user.login,
            response.webUrl
        )
    }
}