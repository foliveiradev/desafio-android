package oliveiradev.com.github.concrete_desafio_android.helper

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by felipe on 20/02/18.
 */
fun Date.format(pattern: String): String {
    val formatter = SimpleDateFormat(pattern)
    return formatter.format(this)
}