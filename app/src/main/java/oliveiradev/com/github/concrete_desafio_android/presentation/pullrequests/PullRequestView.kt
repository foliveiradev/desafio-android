package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import oliveiradev.com.github.concrete_desafio_android.presentation.base.EmptyStateView
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadView

/**
 * Created by felipe on 20/02/18.
 */
interface PullRequestView : LoadView, EmptyStateView, PullRequestErrorView {

    fun addItemOnList(pullRequestUIModel: PullRequestUIModel)
    fun putOpenedPRQuantity(quantity: Int)
    fun navigateToPullRequestBrowser(url: String)
}