package oliveiradev.com.github.concrete_desafio_android.helper

import android.databinding.BindingAdapter
import android.view.View

/**
 * Created by felipe on 20/02/18.
 */
@BindingAdapter("isVisible")
fun View.isVisible(predicate: Boolean) {
    this.visibility = if (predicate) View.VISIBLE else View.GONE
}