package oliveiradev.com.github.concrete_desafio_android.data.repositories

import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse

/**
 * Created by felipe on 18/02/18.
 */
interface RepositoryDataSource {

    fun getRepositories(page: Int): Flowable<RepositoryResponse>
    fun saveInternal(repository: RepositoryResponse)
    val itemsCount: Int
}