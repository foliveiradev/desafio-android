package oliveiradev.com.github.concrete_desafio_android.helper.schedulers

import io.reactivex.Scheduler

/**
 * Created by felipe on 25/02/18.
 */
interface BaseScheduler {

    fun io(): Scheduler
    fun ui(): Scheduler
}