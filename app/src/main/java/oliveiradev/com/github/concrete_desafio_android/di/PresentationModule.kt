package oliveiradev.com.github.concrete_desafio_android.di

import com.github.salomonbrys.kodein.*
import com.github.salomonbrys.kodein.android.androidActivityScope
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestResponse
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse
import oliveiradev.com.github.concrete_desafio_android.helper.schedulers.BaseScheduler
import oliveiradev.com.github.concrete_desafio_android.helper.schedulers.OrdinaryScheduler
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadBehavior
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadView
import oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests.PullRequestPresenter
import oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests.PullRequestUIMapper
import oliveiradev.com.github.concrete_desafio_android.presentation.repositories.RepositoryPresenter
import oliveiradev.com.github.concrete_desafio_android.presentation.repositories.RepositoryUIMapper

/**
 * Created by felipe on 18/02/18.
 */
object PresentationModule {

    val presentationModule = Kodein.Module {

        bind<BaseScheduler>(ORDINARY_SCHEDULER) with provider {
            OrdinaryScheduler()
        }

        bind<RepositoryUIMapper>() with provider {
            RepositoryUIMapper()
        }

        bind<LoadBehavior<RepositoryResponse>>() with factory { loadView: LoadView ->
            LoadBehavior<RepositoryResponse>(loadView)
        }

        bind<RepositoryPresenter>() with scopedSingleton(androidActivityScope) {
            RepositoryPresenter(
                instance(),
                instance(),
                with(it as LoadView).instance(),
                instance(ORDINARY_SCHEDULER)
            )
        }

        bind<PullRequestUIMapper>() with provider {
            PullRequestUIMapper()
        }

        bind<LoadBehavior<PullRequestResponse>>() with factory { loadView: LoadView ->
            LoadBehavior<PullRequestResponse>(loadView)
        }

        bind<PullRequestPresenter>() with scopedSingleton(androidActivityScope) {
            PullRequestPresenter(
                instance(),
                instance(),
                with(it as LoadView).instance(),
                instance(ORDINARY_SCHEDULER)
            )
        }
    }

    private const val ORDINARY_SCHEDULER = "ordinaryScheduler"
}