package oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by felipe on 20/02/18.
 */
interface PullRequestAPI {

    @GET("repos/{owner}/{repo}/pulls")
    fun getPullRequests(@Path("owner") owner: String, @Path("repo") repo: String): Single<List<PullRequestResponse>>
}