package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import io.reactivex.functions.Function
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse

/**
 * Created by felipe on 18/02/18.
 */
class RepositoryUIMapper : Function<RepositoryResponse, RepositoryUIModel> {

    override fun apply(response: RepositoryResponse): RepositoryUIModel {
        return RepositoryUIModel(
                response.name,
                response.description,
                response.starsCount.toString(),
                response.forksCount.toString(),
                response.owner.login,
                response.owner.avatar
        )
    }
}