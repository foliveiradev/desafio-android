package oliveiradev.com.github.concrete_desafio_android.data.repositories.local

import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.data.repositories.RepositoryDataSource
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse

class RepositoryLocalService(private val dao: RepositoryDAO) : RepositoryDataSource {

    override fun getRepositories(page: Int): Flowable<RepositoryResponse> {
        return dao.getRepositories()
            .flatMapPublisher { Flowable.fromIterable(it) }
            .map { repositoryEntityToResponse(it) }
    }

    override fun saveInternal(repository: RepositoryResponse) {
        dao.addRepository(repositoryResponseToEntity(repository))
    }

    override val itemsCount: Int
        get() = dao.repositorySize()
}