package oliveiradev.com.github.concrete_desafio_android.data.repositories.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface RepositoryDAO {

    // TODO implements offset to pagination
    @Query("SELECT * FROM RepositoryEntity ORDER BY starsCount DESC")
    fun getRepositories(): Single<List<RepositoryEntity>>

    @Query("SELECT count(*) FROM RepositoryEntity")
    fun repositorySize(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addRepository(repositoryEntity: RepositoryEntity)
}