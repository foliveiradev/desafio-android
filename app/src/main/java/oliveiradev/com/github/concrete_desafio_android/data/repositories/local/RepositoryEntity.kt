package oliveiradev.com.github.concrete_desafio_android.data.repositories.local

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class RepositoryEntity(
    @PrimaryKey val id: Long,
    val name: String,
    val description: String,
    val starsCount: Int,
    val forksCount: Int,
    val login: String,
    val avatar: String
)