package oliveiradev.com.github.concrete_desafio_android.data

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by felipe on 18/02/18.
 */
object ServiceFactory {

    inline fun <reified T> create(baseUrl: String, httpClient: OkHttpClient) : T {
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create(T::class.java)
    }
}