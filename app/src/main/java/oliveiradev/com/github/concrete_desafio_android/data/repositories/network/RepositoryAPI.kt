package oliveiradev.com.github.concrete_desafio_android.data.repositories.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by felipe on 18/02/18.
 */
interface RepositoryAPI {

    @GET("search/repositories?q=language:Java&sort=stars")
    fun getRepositories(@Query("page") page: Int): Single<ItemsResponse<RepositoryResponse>>
}