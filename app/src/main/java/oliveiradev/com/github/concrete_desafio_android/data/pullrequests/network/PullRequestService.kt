package oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network

import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.PullRequestDataSource

/**
 * Created by felipe on 20/02/18.
 */
class PullRequestService(private val pullRequestAPI: PullRequestAPI): PullRequestDataSource {

    override fun getPullRequests(owner: String, repo: String): Flowable<PullRequestResponse> {
        return pullRequestAPI.getPullRequests(owner, repo).flatMapPublisher {
            Flowable.fromIterable(it)
        }
    }
}