package oliveiradev.com.github.concrete_desafio_android.helper.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by felipe on 25/02/18.
 */
class OrdinaryScheduler : BaseScheduler {

    override fun io(): Scheduler = Schedulers.io()

    override fun ui(): Scheduler = AndroidSchedulers.mainThread()
}