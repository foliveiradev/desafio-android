package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by felipe on 18/02/18.
 */
data class RepositoryUIModel(
    val name: String,
    val description: String,
    val starsCount: String,
    val forksCount: String,
    val ownerName: String,
    val avatarUrl: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(starsCount)
        parcel.writeString(forksCount)
        parcel.writeString(ownerName)
        parcel.writeString(avatarUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RepositoryUIModel> {
        override fun createFromParcel(parcel: Parcel): RepositoryUIModel {
            return RepositoryUIModel(parcel)
        }

        override fun newArray(size: Int): Array<RepositoryUIModel?> {
            return arrayOfNulls(size)
        }
    }
}