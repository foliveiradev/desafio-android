package oliveiradev.com.github.concrete_desafio_android.data.repositories.network

/**
 * Created by felipe on 18/02/18.
 */
data class ItemsResponse<out T> (val items: List<T>)