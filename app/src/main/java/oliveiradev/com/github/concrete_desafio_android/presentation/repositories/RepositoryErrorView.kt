package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

/**
 * Created by felipe on 25/02/18.
 */
interface RepositoryErrorView {

    fun showGetRepositoriesError()
}