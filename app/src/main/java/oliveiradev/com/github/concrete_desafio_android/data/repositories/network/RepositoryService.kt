package oliveiradev.com.github.concrete_desafio_android.data.repositories.network

import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.data.repositories.RepositoryDataSource

/**
 * Created by felipe on 18/02/18.
 */
class RepositoryService(private val api: RepositoryAPI) : RepositoryDataSource {

    override fun getRepositories(page: Int): Flowable<RepositoryResponse> {
        return api.getRepositories(page).flatMapPublisher {
            Flowable.fromIterable(it.items)
        }
    }

    override fun saveInternal(repository: RepositoryResponse) {}

    override val itemsCount: Int = 0
}