package oliveiradev.com.github.concrete_desafio_android.helper

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by felipe on 19/02/18.
 */
@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(url: String?) {
    Picasso.with(context).load(url).fit().centerCrop().into(this)
}