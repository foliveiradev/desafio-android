package oliveiradev.com.github.concrete_desafio_android.data.pullrequests

import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestResponse

/**
 * Created by felipe on 20/02/18.
 */
class PullRequestRepository(private val remotePullRequestDataSource: PullRequestDataSource): PullRequestDataSource {

    override fun getPullRequests(owner: String, repo: String): Flowable<PullRequestResponse> {
        return remotePullRequestDataSource.getPullRequests(owner, repo)
    }
}