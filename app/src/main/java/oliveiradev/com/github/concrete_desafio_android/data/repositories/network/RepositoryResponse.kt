package oliveiradev.com.github.concrete_desafio_android.data.repositories.network

import com.google.gson.annotations.SerializedName

/**
 * Created by felipe on 18/02/18.
 */
data class RepositoryResponse(
        val id: Long,
        val name: String,
        val description: String,
        @SerializedName("stargazers_count") val starsCount: Int,
        @SerializedName("forks_count") val forksCount: Int,
        val owner: OwnerResponse
)