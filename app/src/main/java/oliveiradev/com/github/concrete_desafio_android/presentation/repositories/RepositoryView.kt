package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import oliveiradev.com.github.concrete_desafio_android.presentation.base.EmptyStateView
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadView

/**
 * Created by felipe on 18/02/18.
 */
interface RepositoryView : LoadView, EmptyStateView, RepositoryErrorView {

    fun addItemOnList(repositoryUIModel: RepositoryUIModel)
    fun navigateToPullRequests(repositoryUIModel: RepositoryUIModel)
}