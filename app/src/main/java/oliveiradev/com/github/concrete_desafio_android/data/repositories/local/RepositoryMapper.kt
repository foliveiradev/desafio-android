package oliveiradev.com.github.concrete_desafio_android.data.repositories.local

import io.reactivex.functions.Function
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.OwnerResponse
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse

fun repositoryEntityToResponse(repositoryEntity: RepositoryEntity): RepositoryResponse {
    val func: Function<RepositoryEntity, RepositoryResponse> = Function {
        RepositoryResponse(
            it.id,
            it.name,
            it.description,
            it.starsCount,
            it.forksCount,
            OwnerResponse(it.login, it.avatar)
        )
    }

    return func.apply(repositoryEntity)
}

fun repositoryResponseToEntity(repositoryResponse: RepositoryResponse): RepositoryEntity {
    val func: Function<RepositoryResponse, RepositoryEntity> = Function {
        RepositoryEntity(
            it.id,
            it.name,
            it.description,
            it.starsCount,
            it.forksCount,
            it.owner.login,
            it.owner.avatar
        )
    }

    return func.apply(repositoryResponse)
}