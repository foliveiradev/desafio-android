package oliveiradev.com.github.concrete_desafio_android

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.conf.ConfigurableKodein
import com.squareup.leakcanary.LeakCanary
import oliveiradev.com.github.concrete_desafio_android.di.DataModule
import oliveiradev.com.github.concrete_desafio_android.di.PresentationModule

/**
 * Created by felipe on 18/02/18.
 */
class GithubApp : Application(), KodeinAware {

    override val kodein = ConfigurableKodein(mutable = true)

    override fun onCreate() {
        super.onCreate()
        configDependencies()
        initializeLeakDetect()
        Stetho.initializeWithDefaults(this)
    }

    private fun configDependencies() {
        kodein.addImport(DataModule(this).dataModule, true)
        kodein.addImport(PresentationModule.presentationModule)
    }

    private fun initializeLeakDetect() {
        LeakCanary.install(this)
    }
}

fun Context.asApp() = this.applicationContext as GithubApp
