package oliveiradev.com.github.concrete_desafio_android.presentation.base

/**
 * Created by felipe on 23/02/18.
 */
interface BaseAdapter<T> {

    fun addItem(item: T)
    fun addAll(items: List<T>)
    fun getItems(): ArrayList<T>
}