package oliveiradev.com.github.concrete_desafio_android.presentation.repositories

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import oliveiradev.com.github.concrete_desafio_android.databinding.ItemRepositoryBinding
import oliveiradev.com.github.concrete_desafio_android.presentation.base.BaseAdapter

/**
 * Created by felipe on 18/02/18.
 */
typealias OnRepositoryClickListener = (RepositoryUIModel) -> Unit

class RepositoryAdapter(private val onItemClickListener: OnRepositoryClickListener) :
    RecyclerView.Adapter<RepositoryViewHolder>(), BaseAdapter<RepositoryUIModel> {

    private val items: MutableList<RepositoryUIModel> by lazy { mutableListOf<RepositoryUIModel>() }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoryViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        return RepositoryViewHolder(ItemRepositoryBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RepositoryViewHolder?, position: Int) {
        val repo = items[position]
        holder?.apply {
            bind(repo)
            binding.container.setOnClickListener { onItemClickListener(repo) }
        }
    }

    override fun addItem(item: RepositoryUIModel) {
        items.add(item)
        notifyItemInserted(itemCount - 1)
    }

    override fun addAll(items: List<RepositoryUIModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItems(): ArrayList<RepositoryUIModel> {
        return ArrayList(items)
    }
}

class RepositoryViewHolder(val binding: ItemRepositoryBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(repo: RepositoryUIModel) {
        binding.repository = repo
        binding.executePendingBindings()
    }
}
