package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ObservableBoolean
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import oliveiradev.com.github.concrete_desafio_android.R
import oliveiradev.com.github.concrete_desafio_android.databinding.ActivityPullrequestsBinding
import oliveiradev.com.github.concrete_desafio_android.presentation.repositories.RepositoryUIModel

/**
 * Created by felipe on 20/02/18.
 */
class PullRequestsActivity : AppCompatActivity(), PullRequestView {

    private val kodein = LazyKodein(appKodein)
    private val repositoryUIModel by lazy {
        intent.getParcelableExtra<RepositoryUIModel>(
            REPOSITORY_EXTRA
        )
    }
    private val binding: ActivityPullrequestsBinding by lazy {
        DataBindingUtil.setContentView<ActivityPullrequestsBinding>(
            this,
            R.layout.activity_pullrequests
        )
    }
    private val presenter by kodein.with(this).instance<PullRequestPresenter>()
    private val pullrequestAdapter by lazy {
        PullRequestAdapter { presenter.navigateToPullRequestBrowser(it) }
    }
    private val loadingObserver: ObservableBoolean by lazy { ObservableBoolean(true) }
    private val emptyStateObserver: ObservableBoolean by lazy { ObservableBoolean(false) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(presenter)
        setupView(binding)

        if (savedInstanceState == null) {
            presenter.apply {
                attachView(this@PullRequestsActivity)
                getPullRequests(repositoryUIModel)
            }
        } else {
            presenter.attachView(this)
            pullrequestAdapter.addAll(savedInstanceState.getParcelableArrayList(PULLREQUESTS_STATE))
            loadingObserver.set(false)
            checkIfAdapterIsEmpty()
            putOpenedPRQuantity(pullrequestAdapter.itemCount)
        }
    }

    override fun navigateUpTo(upIntent: Intent?): Boolean {
        finish()
        return true
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelableArrayList(PULLREQUESTS_STATE, pullrequestAdapter.getItems())
    }

    override fun showLoading() {
        loadingObserver.set(true)
    }

    override fun dismissLoading() {
        loadingObserver.set(false)
    }

    override fun showEmptyState() {
        emptyStateObserver.set(true)
    }

    override fun dismissEmptyState() {
        emptyStateObserver.set(false)
    }

    override fun addItemOnList(pullRequestUIModel: PullRequestUIModel) {
        pullrequestAdapter.addItem(pullRequestUIModel)
    }

    override fun putOpenedPRQuantity(quantity: Int) {
        binding.openedPRs = quantity
    }

    override fun navigateToPullRequestBrowser(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }

    override fun showGetPullRequestsError() {
        Toast.makeText(this, R.string.pull_request_error, Toast.LENGTH_LONG).show()
    }

    private fun setupView(binding: ActivityPullrequestsBinding) = with(binding) {
        isLoading = loadingObserver
        isEmptyState = emptyStateObserver

        supportActionBar?.apply {
            title = repositoryUIModel.name
        }

        pullRequests.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@PullRequestsActivity)
            adapter = pullrequestAdapter
        }
    }

    private fun checkIfAdapterIsEmpty() {
        if (pullrequestAdapter.itemCount == 0) showEmptyState()
        else dismissEmptyState()
    }

    companion object {

        private const val PULLREQUESTS_STATE = "pullrequestsState"
        const val REPOSITORY_EXTRA = "repositoryExtra"

        fun createIntent(context: Context, repositoryUIModel: RepositoryUIModel): Intent {
            return Intent(context, PullRequestsActivity::class.java)
                .putExtra(REPOSITORY_EXTRA, repositoryUIModel)
        }
    }
}