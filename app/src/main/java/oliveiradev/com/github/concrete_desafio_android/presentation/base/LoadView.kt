package oliveiradev.com.github.concrete_desafio_android.presentation.base

/**
 * Created by felipe on 20/02/18.
 */
interface LoadView {

    fun showLoading()
    fun dismissLoading()
}