package oliveiradev.com.github.concrete_desafio_android.helper

import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import oliveiradev.com.github.concrete_desafio_android.helper.schedulers.BaseScheduler

/**
 * Created by felipe on 18/02/18.
 */
fun <T> Flowable<T>.fromIOToMainThread(baseScheduler: BaseScheduler): Flowable<T> {
    return compose { upstream ->
        upstream
            .subscribeOn(baseScheduler.io())
            .observeOn(baseScheduler.ui())
    }
}
