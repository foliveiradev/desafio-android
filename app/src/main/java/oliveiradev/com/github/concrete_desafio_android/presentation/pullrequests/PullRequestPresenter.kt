package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import io.reactivex.disposables.CompositeDisposable
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.PullRequestRepository
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestResponse
import oliveiradev.com.github.concrete_desafio_android.helper.fromIOToMainThread
import oliveiradev.com.github.concrete_desafio_android.helper.schedulers.BaseScheduler
import oliveiradev.com.github.concrete_desafio_android.presentation.base.BasePresenter
import oliveiradev.com.github.concrete_desafio_android.presentation.base.LoadBehavior
import oliveiradev.com.github.concrete_desafio_android.presentation.repositories.RepositoryUIModel

/**
 * Created by felipe on 18/02/18.
 */
class PullRequestPresenter(
    private val pullRequestRepository: PullRequestRepository,
    private val pullRequestUIMapper: PullRequestUIMapper,
    private val loadBehavior: LoadBehavior<PullRequestResponse>,
    private val baseScheduler: BaseScheduler
) : BasePresenter<PullRequestView>() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getPullRequests(repositoryUIModel: RepositoryUIModel) {
        compositeDisposable.add(
            pullRequestRepository.getPullRequests(
                repositoryUIModel.ownerName,
                repositoryUIModel.name
            )
                .fromIOToMainThread(baseScheduler)
                .compose(loadBehavior)
                .map(pullRequestUIMapper)
                .doOnNext { view?.addItemOnList(it) }
                .toList()
                .subscribe(
                    { PRs ->
                        view?.putOpenedPRQuantity(PRs.size)
                        checkPRsSize(PRs)
                    },
                    { view?.showGetPullRequestsError() }
                )
        )
    }

    private fun checkPRsSize(PRs: List<PullRequestUIModel>) {
        if (PRs.isEmpty()) {
            view?.showEmptyState()
        } else {
            view?.dismissEmptyState()
        }
    }

    fun navigateToPullRequestBrowser(pullRequestUIModel: PullRequestUIModel) {
        view?.navigateToPullRequestBrowser(pullRequestUIModel.webUrl)
    }

    override fun stop() {
        compositeDisposable.clear()
    }
}