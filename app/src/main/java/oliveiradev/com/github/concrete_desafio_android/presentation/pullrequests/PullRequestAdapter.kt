package oliveiradev.com.github.concrete_desafio_android.presentation.pullrequests

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import oliveiradev.com.github.concrete_desafio_android.databinding.ItemPullRequestBinding
import oliveiradev.com.github.concrete_desafio_android.presentation.base.BaseAdapter

        /**
         * Created by felipe on 18/02/18.
         */
typealias OnPullRequestClickListener = (PullRequestUIModel) -> Unit

class PullRequestAdapter(private val onItemClickListener: OnPullRequestClickListener) :
    RecyclerView.Adapter<PullRequestViewHolder>(), BaseAdapter<PullRequestUIModel> {

    private val items: MutableList<PullRequestUIModel> by lazy { mutableListOf<PullRequestUIModel>() }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PullRequestViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        return PullRequestViewHolder(ItemPullRequestBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PullRequestViewHolder?, position: Int) {
        val pullrequest = items[position]
        holder?.apply {
            bind(pullrequest)
            binding.container.setOnClickListener { onItemClickListener(pullrequest) }
        }
    }

    override fun addItem(item: PullRequestUIModel) {
        items.add(item)
        notifyItemInserted(itemCount - 1)
    }

    override fun addAll(items: List<PullRequestUIModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItems(): ArrayList<PullRequestUIModel> {
        return ArrayList(items)
    }
}

class PullRequestViewHolder(val binding: ItemPullRequestBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(pullrequest: PullRequestUIModel) {
        binding.pullrequest = pullrequest
        binding.executePendingBindings()
    }
}
