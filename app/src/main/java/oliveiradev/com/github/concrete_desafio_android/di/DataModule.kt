package oliveiradev.com.github.concrete_desafio_android.di

import android.content.Context
import com.github.salomonbrys.kodein.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import oliveiradev.com.github.concrete_desafio_android.BuildConfig
import oliveiradev.com.github.concrete_desafio_android.data.GithubDataBase
import oliveiradev.com.github.concrete_desafio_android.data.ServiceFactory
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.PullRequestDataSource
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.PullRequestRepository
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestAPI
import oliveiradev.com.github.concrete_desafio_android.data.pullrequests.network.PullRequestService
import oliveiradev.com.github.concrete_desafio_android.data.repositories.GithubRepoRepository
import oliveiradev.com.github.concrete_desafio_android.data.repositories.RepositoryDataSource
import oliveiradev.com.github.concrete_desafio_android.data.repositories.local.RepositoryDAO
import oliveiradev.com.github.concrete_desafio_android.data.repositories.local.RepositoryLocalService
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryAPI
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryService

/**
 * Created by felipe on 18/02/18.
 */
class DataModule(context: Context) {

    val dataModule = Kodein.Module(allowSilentOverride = true) {

        bind<Interceptor>() with provider {
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        }

        bind<OkHttpClient>() with provider {
            OkHttpClient.Builder()
                    .addInterceptor(instance())
                    .build()
        }

        bind<RepositoryAPI>() with provider {
            ServiceFactory.create<RepositoryAPI>(BuildConfig.GITHUB_BASE_URL, instance())
        }

        bind<RepositoryDAO>() with singleton {
            GithubDataBase.getInstance(context)
                .getRepositoryDAO()
        }

        bind<RepositoryDataSource>(LOCAL) with singleton {
            RepositoryLocalService(instance())
        }

        bind<RepositoryDataSource>(NETWORK) with singleton {
            RepositoryService(instance())
        }

        bind<GithubRepoRepository>() with provider {
            GithubRepoRepository(instance(NETWORK), instance(LOCAL))
        }

        bind<PullRequestAPI>() with provider {
            ServiceFactory.create<PullRequestAPI>(BuildConfig.GITHUB_BASE_URL, instance())
        }

        bind<PullRequestDataSource>() with singleton {
            PullRequestService(instance())
        }

        bind<PullRequestRepository>() with provider {
            PullRequestRepository(instance())
        }
    }

    companion object {

        private const val LOCAL = "local"
        private const val NETWORK = "network"
    }
}