package oliveiradev.com.github.concrete_desafio_android.data.repositories

import io.reactivex.Flowable
import oliveiradev.com.github.concrete_desafio_android.data.repositories.network.RepositoryResponse

/**
 * Created by felipe on 18/02/18.
 */
class GithubRepoRepository(
    private val remoteRepositoryDataSource: RepositoryDataSource,
    private val localRepositoryDataSource: RepositoryDataSource
) : RepositoryDataSource {

    override fun getRepositories(page: Int): Flowable<RepositoryResponse> {
        return if (existsCache()) {
            localRepositoryDataSource.getRepositories(page)
        } else {
            remoteRepositoryDataSource.getRepositories(page)
                .doOnNext { saveInternal(it) }
        }
    }

    override fun saveInternal(repository: RepositoryResponse) {
        localRepositoryDataSource.saveInternal(repository)
    }

    override val itemsCount: Int
        get() = localRepositoryDataSource.itemsCount

    private fun existsCache(): Boolean {
        return itemsCount > 0
    }
}